﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Security.Cryptography
Imports System.IO

Namespace InterfazCupones.Clases

    Class DataEncryption

        Private Shared Function AES_Encrypt(ByVal bytesToBeEncrypted As Byte(), ByVal passwordBytes As Byte()) As Byte()
            Dim encryptedBytes As Byte() = Nothing
            Dim saltBytes As Byte() = New Byte() {10, 221, 54, 32, 77, 3, 195, 175}

            Using ms As MemoryStream = New MemoryStream()

                Using AES As RijndaelManaged = New RijndaelManaged()
                    AES.KeySize = 256
                    AES.BlockSize = 128
                    Dim key = New Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000)
                    AES.Key = key.GetBytes(AES.KeySize / 8)
                    AES.IV = key.GetBytes(AES.BlockSize / 8)
                    AES.Mode = CipherMode.CBC

                    Using cs = New CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write)
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length)
                        cs.Close()
                    End Using

                    encryptedBytes = ms.ToArray()
                End Using
            End Using

            Return encryptedBytes
        End Function

        Private Shared Function AES_Decrypt(ByVal bytesToBeDecrypted As Byte(), ByVal passwordBytes As Byte()) As Byte()
            Dim decryptedBytes As Byte() = Nothing
            Dim saltBytes As Byte() = New Byte() {10, 221, 54, 32, 77, 3, 195, 175}

            Using ms As MemoryStream = New MemoryStream()

                Using AES As RijndaelManaged = New RijndaelManaged()
                    AES.KeySize = 256
                    AES.BlockSize = 128
                    Dim key = New Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000)
                    AES.Key = key.GetBytes(AES.KeySize / 8)
                    AES.IV = key.GetBytes(AES.BlockSize / 8)
                    AES.Mode = CipherMode.CBC

                    Using cs = New CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write)
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length)
                        cs.Close()
                    End Using

                    decryptedBytes = ms.ToArray()
                End Using
            End Using

            Return decryptedBytes
        End Function

        Public Shared Function EncryptText(ByVal Input As String, ByVal Key As String) As String

            Try
                Dim BytesToBeEncrypted As Byte() = Encoding.Unicode.GetBytes(Input)
                Dim PasswordBytes As Byte() = Encoding.Unicode.GetBytes(Key)
                PasswordBytes = SHA256.Create().ComputeHash(PasswordBytes)
                Dim BytesEncrypted As Byte() = AES_Encrypt(BytesToBeEncrypted, PasswordBytes)
                Dim Result As String = Functions.ByteToHex2(BytesEncrypted)
                Return result
            Catch Any As Exception
                Return Input
            End Try

        End Function

        Public Shared Function DecryptText(ByVal Input As String, ByVal Key As String) As String

            Try
                Dim BytesToBeDecrypted As Byte() = Functions.Hex2ToByte(Input)
                Dim PasswordBytes As Byte() = Encoding.Unicode.GetBytes(Key)
                PasswordBytes = SHA256.Create().ComputeHash(PasswordBytes)
                Dim BytesDecrypted As Byte() = AES_Decrypt(BytesToBeDecrypted, PasswordBytes)
                Dim Result As String = Encoding.Unicode.GetString(BytesDecrypted)
                Return Result
            Catch Any As Exception
                Return Input
            End Try

        End Function

        Public Shared Function EncryptTextADV(ByVal Input As String, ByVal Key As String) As String

            Try
                Dim BytesToBeEncrypted As Byte() = Encoding.Unicode.GetBytes(Input)
                Dim PasswordBytes As Byte() = Encoding.Unicode.GetBytes(Key)
                PasswordBytes = SHA512.Create().ComputeHash(PasswordBytes)
                Dim BytesEncrypted As Byte() = AES_Encrypt(BytesToBeEncrypted, PasswordBytes)
                Dim Result As String = Functions.ByteToHex2(BytesEncrypted)
                Return Result
            Catch Any As Exception
                Return Input
            End Try

        End Function

        Public Shared Function DecryptTextADV(ByVal Input As String, ByVal Key As String) As String

            Try
                Dim BytesToBeDecrypted As Byte() = Functions.Hex2ToByte(Input)
                Dim PasswordBytes As Byte() = Encoding.Unicode.GetBytes(Key)
                PasswordBytes = SHA512.Create().ComputeHash(PasswordBytes)
                Dim BytesDecrypted As Byte() = AES_Decrypt(BytesToBeDecrypted, PasswordBytes)
                Dim Result As String = Encoding.Unicode.GetString(BytesDecrypted)
                Return Result
            Catch Any As Exception
                Return Input
            End Try

        End Function

    End Class

End Namespace
