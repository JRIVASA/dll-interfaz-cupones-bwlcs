﻿Imports System.Net
Imports System.IO
Imports Newtonsoft.Json
Imports InterfazCupones.InterfazCupones.Clases

Public Class JsonResp_TipoCupon
    Public couponCode As String
    Public description As String
    Public couponType As String
    Public ballanceAllow As Boolean
    Public autoID As Int64
End Class

Public Class JsonResp_AgregarCupon
    Public couponNumber As String
    Public couponCode As String
    Public systemNumber As String
    Public accountCode As String
    Public locationCodeOrigin As String
    Public posCodeOrigin As String
    Public creationDate As Date
    Public validityDateStart As Date
    Public validityDateEnd As Date
    Public currencyCodeISO As String
    Public currencyCodeSystem As String
    Public currencyBaseAmount As String
    Public currencyUsedAmount As String
    Public couponStatus As String
    Public exchangeRate As String
    Public autoID As Int64
End Class

Public Class JsonResp_ConsumirCupon
    Public transactionNumber As String
    Public couponNumber As String
    Public channelID As String
    Public locationCodeUsed As String
    Public posCodeUsed As String
    Public transactionTypeUsed As String
    Public documentNumberUsed As String
    Public applicationDate As Date
    Public currencyAmount As String
    Public exchangeRate As String
    Public autoID As Int64
End Class

Public Class JsonResp_Fail
    Public message As String
    Public _error As Boolean
    Public errorMessage As String
End Class

Public Class JsonResp_FailV2
    Public message As String
    Public _error As Boolean
    Public RespCode As String
End Class

Public Class ClassCuponesStellar

    Private Const EncryptionKey As String = "InterfazCupones-Credentials-MinimumSecurity. 0515FD544E4F3S4S8D3XFDLKIE9Q2DV9"

    Private Const TContext = "AK59E9523F1FSDMKJWQ4Q826A6"

    Private DebugMode As Boolean
    Private SharedAppK As String
    Private WhoIAm As String

    Public Host As String

    Public Codigo As String
    Public Mensaje As String
    Public Referencia As String

    Private ApiUser As String
    Private ApiKey As String
    Private ServiceVersion As Double
    Private Locale As String

    Private OverrideSrvKey As String

    Private VB6Aux As Object

    Public Sub New()
        Try
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12 Or SecurityProtocolType.Ssl3)
            ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
        Catch
            'MsgBox(Err.Description, , )
        End Try
    End Sub

    Public Function AcceptAllCertifications(ByVal sender As Object,
    ByVal certification As System.Security.Cryptography.X509Certificates.X509Certificate,
    ByVal chain As System.Security.Cryptography.X509Certificates.X509Chain,
    ByVal sslPolicyErrors As System.Net.Security.SslPolicyErrors) As Boolean
        Return True
    End Function

    Public Property VB6AuxObj() As Object
        Get
            Return VB6Aux
        End Get
        Set(ByVal value As Object)
            VB6Aux = value
        End Set
    End Property

    Public Property Prop_DebugMode() As Boolean
        Get
            Return DebugMode
        End Get
        Set(ByVal value As Boolean)
            DebugMode = value
        End Set
    End Property

    Public Property Prop_SAK() As String
        Get
            Return String.Empty
        End Get
        Set(ByVal value As String)
            If WhoIAm.Equals("513114R") Then
                SharedAppK = value
            Else
                SharedAppK = "NAK"
            End If
        End Set
    End Property

    Public Property Prop_Who() As String
        Get
            Return String.Empty
        End Get
        Set(ByVal value As String)
            WhoIAm = value
        End Set
    End Property

    Public Property Prop_TCX() As String
        Get
            Return TContext
        End Get
        Set(ByVal value As String)
            'TContext = 1
        End Set
    End Property

    Public Property Prop_ApiUser() As String
        Get
            Return String.Empty
        End Get
        Set(ByVal value As String)
            If WhoIAm.Equals("513114R") Then
                ApiUser = value
            Else
                ApiUser = "NAK"
            End If
        End Set
    End Property

    Public Property Prop_ApiKey() As String
        Get
            Return String.Empty
        End Get
        Set(ByVal value As String)
            If WhoIAm.Equals("513114R") Then
                ApiKey = value
            Else
                ApiKey = "NAK"
            End If
        End Set
    End Property

    Public Property Prop_ServiceVersion() As Double
        Get
            Return ServiceVersion
        End Get
        Set(ByVal value As Double)
            ServiceVersion = value
        End Set
    End Property

    Public Property Prop_Locale() As String
        Get
            Return Locale
        End Get
        Set(ByVal value As String)
            Locale = value
        End Set
    End Property

    Public Property Prop_OverrideSrvKey() As String
        Get
            Return String.Empty
        End Get
        Set(ByVal value As String)
            If WhoIAm.Equals("513114R") Then
                OverrideSrvKey = value
            Else
                OverrideSrvKey = String.Empty
            End If
        End Set
    End Property

    Private Sub LimpiarVariables()

        Try

            Codigo = String.Empty
            Mensaje = String.Empty
            Referencia = String.Empty

        Catch ex As Exception
            Mensaje = "Error Limpiar Variables" + vbNewLine + "(" + Err.Number.ToString() + ") " + Err.Description
        End Try

    End Sub

    Public Function AgregarTipoCupon(ByVal pCodigoCuenta As String, ByVal pDescripcion As String, pTipo As String,
    Optional ByVal pPermitirConsumoParcial As Boolean = False) As Boolean

        Dim Link
        Dim Request As WebRequest
        Dim Response As WebResponse
        Dim Data As String, RawData As String

        Dim RespHttp As HttpStatusCode

        Try

            LimpiarVariables()

            Link = Host & "/coupon_type"

            Request = WebRequest.Create(Link)
            Request.Method = "POST"
            Request.ContentType = "application/json"
            Request.Timeout = 30000

            RawData = ("{ " &
            """ServiceVersion"":""" & ServiceVersion.ToString & """, " &
            """AccountCode"":""" & pCodigoCuenta & """,  " &
            """Description"":""" & pDescripcion & """, " &
            """CouponType"":""" & pTipo & """, " &
            """BallanceAllow"":" & LCase(IIf(pPermitirConsumoParcial, "true", "false")) & ", " &
            """Locale"":""" & Locale & """, " &
            """APIUser"":""" & ApiUser & """, " &
            """APIKey"":""" & ApiKey & """, " &
            """Xenotransplantation"":""P8Q3AF7W6D3A6D220"" " &
            "}")

            Data = "{" &
            """Request"":""Initialize"", " &
            """Ancestor"":""Standard"", " &
            """Payload"":""" & DataEncryption.EncryptTextADV(RawData, IIf(OverrideSrvKey.Trim.Length <= 0, EncryptionKey + SharedAppK, OverrideSrvKey)) & """ " &
            "}"

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Replace(RawData, ", " &
                """APIUser"":""" & ApiUser & """, " &
                """APIKey"":""" & ApiKey & """, " &
                """Xenotransplantation"":""P8Q3AF7W6D3A6D220""",
                String.Empty, 1, 1, CompareMethod.Text))
                'VB6Aux.ShowBigDebugMessage(RawData)
                'VB6Aux.ShowBigDebugMessage(Data)
            End If

            RawData = String.Empty

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(Data)

            Using requestStream As Stream = Request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using

            Response = Request.GetResponse

            RespHttp = CType(Response, HttpWebResponse).StatusCode

            If DebugMode Then
                MsgBox(RespHttp.ToString(), MsgBoxStyle.Information, "DebugMode - Resp. HttpStatusCode")
            End If

            If RespHttp = HttpStatusCode.Created Then

                Dim dataStream As New StreamReader(Response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JsonResp_TipoCupon
                resp = JsonConvert.DeserializeObject(Of JsonResp_TipoCupon)(result)

                Codigo = "00"
                Mensaje = "Tipo de Cupon creado exitosamente."

                'Referencia = resp.numeroReferencia

                Return True

            Else

                Dim DataStream As New StreamReader(Response.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                If (ServiceVersion < 2) Then

                    Dim respV1 As JsonResp_Fail
                    respV1 = JsonConvert.DeserializeObject(Of JsonResp_Fail)(Result)

                    If (respV1._error) Then
                        Codigo = "99" ' Temporal mientras se pide el codigo.
                        Mensaje = respV1.message + IIf(Not String.IsNullOrEmpty(respV1.errorMessage), " - " + respV1.errorMessage, String.Empty)
                    Else
                        Codigo = "10" ' Temporal mientras se pide el codigo.
                        Mensaje = respV1.message + IIf(Not String.IsNullOrEmpty(respV1.errorMessage), " - " + respV1.errorMessage, String.Empty)
                    End If

                ElseIf (ServiceVersion >= 2) Then

                    Dim RespV2 As JsonResp_FailV2

                    RespV2 = JsonConvert.DeserializeObject(Of JsonResp_FailV2)(Result)

                    Codigo = RespV2.RespCode
                    Mensaje = RespV2.message

                End If

                Return False

            End If

        Catch ex As Exception

            Codigo = "-1"
            Mensaje = "Falla de comunicación o timeout al intentar crear el tipo de cupón. " +
            "Información adicional: " + vbNewLine + vbNewLine + Err.Description + " [" + Err.Number.ToString() + "]"

            Return False

        End Try

    End Function

    Public Function AgregarCupon(pCodigoCupon As String, pSerialCupon As String, pCodigoCuenta As String,
                                 pCodigoLocalidad As String, pCodOrigen As String, pFechaInicio As String,
                                 pFechaFin As String, pCodMoneda As String, pCodISOMoneda As String,
                                 pMontoBase As String, Optional pExchangeRate As String = "0") As Boolean

        Dim Link
        Dim Request As WebRequest
        Dim Response As WebResponse
        Dim Data As String, RawData As String

        Dim RespHttp As HttpStatusCode

        Try

            LimpiarVariables()

            Link = Host & "/coupon"

            Request = WebRequest.Create(Link)
            Request.Method = "POST"
            Request.ContentType = "application/json"
            Request.Timeout = 30000

            RawData = ("{ " &
            """ServiceVersion"":""" & ServiceVersion.ToString & """, " &
            """CouponCode"":""" & pCodigoCupon & """, " &
            """CouponNumber"":""" & pSerialCupon & """, " &
            """AccountCode"":""" & pCodigoCuenta & """, " &
            """LocationCodeOrigin"":""" & pCodigoLocalidad & """, " &
            """POSCodeOrigin"":""" & pCodOrigen & """, " &
            """ValidityDateStart"":""" & pFechaInicio & """, " &
            """ValidityDateEnd"":""" & pFechaFin & """, " &
            """CurrencyCodeSystem"":""" & pCodMoneda & """, " &
            """ExchangeRate"":""" & pExchangeRate & """, " &
            """CurrencyCodeISO"":""" & pCodISOMoneda & """, " &
            """CurrencyBaseAmount"":""" & pMontoBase & """, " &
            """Locale"":""" & Locale & """, " &
            """APIUser"":""" & ApiUser & """, " &
            """APIKey"":""" & ApiKey & """, " &
            """Ultracalibration"":""5441NJKL654H68UK41E6"" " &
            "}")

            Data = "{" +
            """Request"":""Initialize"", " &
            """Ancestor"":""Standard"", " &
            """Payload"":""" & DataEncryption.EncryptTextADV(RawData, IIf(OverrideSrvKey.Trim.Length <= 0, EncryptionKey + SharedAppK, OverrideSrvKey)) & """ " &
            "}"

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Replace(RawData, ", " &
                """APIUser"":""" & ApiUser & """, " &
                """APIKey"":""" & ApiKey & """, " &
                """Ultracalibration"":""5441NJKL654H68UK41E6""",
                String.Empty, 1, 1, CompareMethod.Text))
                'VB6Aux.ShowBigDebugMessage(EncryptionKey)
                'VB6Aux.ShowBigDebugMessage(SharedAppK)
                'VB6Aux.ShowBigDebugMessage(EncryptionKey + SharedAppK)
                'VB6Aux.ShowBigDebugMessage(RawData)
                'VB6Aux.ShowBigDebugMessage(Data)
            End If

            RawData = String.Empty

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(Data)

            Using requestStream As Stream = Request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using

            Response = Request.GetResponse

            RespHttp = CType(Response, HttpWebResponse).StatusCode

            If DebugMode Then
                MsgBox(RespHttp.ToString(), MsgBoxStyle.Information, "DebugMode - Resp. HttpStatusCode")
            End If

            If RespHttp = HttpStatusCode.Created Then

                Dim dataStream As New StreamReader(Response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JsonResp_AgregarCupon
                resp = JsonConvert.DeserializeObject(Of JsonResp_AgregarCupon)(result)

                Codigo = "00"
                Mensaje = "Cupon activado exitosamente."

                'Referencia = resp.numeroReferencia

                Return True

            Else

                Dim DataStream As New StreamReader(Response.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                If (ServiceVersion < 2) Then

                    Dim respV1 As JsonResp_Fail
                    respV1 = JsonConvert.DeserializeObject(Of JsonResp_Fail)(Result)

                    If (respV1._error) Then
                        Codigo = "99" ' Temporal mientras se pide el codigo.
                        Mensaje = respV1.message + IIf(Not String.IsNullOrEmpty(respV1.errorMessage), " - " + respV1.errorMessage, String.Empty)
                    Else
                        Codigo = "10" ' Temporal mientras se pide el codigo.
                        Mensaje = respV1.message + IIf(Not String.IsNullOrEmpty(respV1.errorMessage), " - " + respV1.errorMessage, String.Empty)
                    End If

                ElseIf (ServiceVersion >= 2) Then

                    Dim RespV2 As JsonResp_FailV2

                    RespV2 = JsonConvert.DeserializeObject(Of JsonResp_FailV2)(Result)

                    Codigo = RespV2.RespCode
                    Mensaje = RespV2.message

                End If

                Return False

            End If

        Catch ex As Exception

            Codigo = "-1"
            Mensaje = "Falla de comunicación o timeout al intentar activar el cupón. " +
            "Información adicional: " + vbNewLine + vbNewLine + Err.Description + " [" + Err.Number.ToString() + "]"
            Return False

        End Try

    End Function

    Public Function ConsumirCupon(pCodigoCupon As String, pSerialCupon As String, pCodigoCuenta As String,
                                  pCanal As String, pCodigoLocalidad As String,
                                  pCodigoPos As String, pTipoTransaccion As String, pNumeroDocumento As String,
                                  pMonto As String, Optional pExchangeRate As String = "1") As Boolean

        Dim Link
        Dim Request As WebRequest
        Dim Response As WebResponse
        Dim Data As String, RawData As String

        Dim RespHttp As HttpStatusCode

        Try

            LimpiarVariables()

            Link = Host & "/use_coupon"

            Request = WebRequest.Create(Link)
            Request.Method = "POST"
            Request.ContentType = "application/json"
            Request.Timeout = 30000

            RawData = ("{ " &
            """ServiceVersion"":""" & ServiceVersion.ToString & """, " &
            """CouponCode"":""" & pCodigoCupon & """, " &
            """CouponNumber"":""" & pSerialCupon & """, " &
            """AccountCode"":""" & pCodigoCuenta & """, " &
            """ChannelID"":""" & pCanal & """, " &
            """LocationCodeUsed"":""" & pCodigoLocalidad & """, " &
            """POSCodeUsed"":""" & pCodigoPos & """, " &
            """TransactionTypeUsed"":""" & pTipoTransaccion & """, " &
            """DocumentNumberUsed"":""" & pNumeroDocumento & """, " &
            """CurrencyAmount"":""" & pMonto & """, " &
            """ExchangeRate"":""" & pExchangeRate & """, " &
            """Locale"":""" & Locale & """, " &
            """APIUser"":""" & ApiUser & """, " &
            """APIKey"":""" & ApiKey & """, " &
            """Underprocrastination"":""4J87329KE389KE98LQL9"" " +
            "}")

            Data = "{" &
            """Request"":""Initialize"", " &
            """Ancestor"":""Standard"", " &
            """Payload"":""" & DataEncryption.EncryptTextADV(RawData, IIf(OverrideSrvKey.Trim.Length <= 0, EncryptionKey + SharedAppK, OverrideSrvKey)) & """ " &
            "}"

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Replace(RawData, ", " &
                """APIUser"":""" & ApiUser & """, " &
                """APIKey"":""" & ApiKey & """, " &
                """Underprocrastination"":""4J87329KE389KE98LQL9""",
                String.Empty, 1, 1, CompareMethod.Text))
                'VB6Aux.ShowBigDebugMessage(RawData)
                'VB6Aux.ShowBigDebugMessage(Data)
            End If

            RawData = String.Empty

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(Data)

            Using requestStream As Stream = Request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using

            Response = Request.GetResponse

            RespHttp = CType(Response, HttpWebResponse).StatusCode

            If DebugMode Then
                MsgBox(RespHttp.ToString(), MsgBoxStyle.Information, "DebugMode - Resp. HttpStatusCode")
            End If

            If RespHttp = HttpStatusCode.Created Then

                Dim dataStream As New StreamReader(Response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JsonResp_ConsumirCupon
                resp = JsonConvert.DeserializeObject(Of JsonResp_ConsumirCupon)(result)

                Codigo = "00"
                Mensaje = "Transaccion Exitosa"
                Referencia = resp.couponNumber

                Return True

            Else

                Dim DataStream As New StreamReader(Response.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                If (ServiceVersion < 2) Then

                    Dim respV1 As JsonResp_Fail
                    respV1 = JsonConvert.DeserializeObject(Of JsonResp_Fail)(Result)

                    If (respV1._error) Then
                        Codigo = "99" ' Temporal mientras se pide el codigo.
                        Mensaje = respV1.message + IIf(Not String.IsNullOrEmpty(respV1.errorMessage), " - " + respV1.errorMessage, String.Empty)
                    Else
                        Codigo = "10" ' Temporal mientras se pide el codigo.
                        Mensaje = respV1.message + IIf(Not String.IsNullOrEmpty(respV1.errorMessage), " - " + respV1.errorMessage, String.Empty)
                    End If

                ElseIf (ServiceVersion >= 2) Then

                    Dim RespV2 As JsonResp_FailV2

                    RespV2 = JsonConvert.DeserializeObject(Of JsonResp_FailV2)(Result)

                    Codigo = RespV2.RespCode
                    Mensaje = RespV2.message

                End If

                Return False

            End If

        Catch ex As Exception

            Codigo = "-1"
            Mensaje = "Falla de comunicación o timeout al intentar utilizar el cupón. " +
            "Información adicional: " + vbNewLine + vbNewLine + Err.Description + " [" + Err.Number.ToString() + "]"
            Return False

        End Try

    End Function

    Public Function AnularConsumoCupon(pCodigoCupon As String, pSerialCupon As String, pCodigoCuenta As String,
                                      pCanal As String, pCodigoLocalidad As String,
                                      pCodigoPos As String, pTipoTransaccion As String, pNumeroDocumento As String,
                                      pMonto As String, Optional pExchangeRate As String = "1") As Boolean

        Dim Link
        Dim Request As WebRequest
        Dim Response As WebResponse
        Dim Data As String, RawData As String

        Dim RespHttp As HttpStatusCode

        Try

            LimpiarVariables()

            Link = Host & "/undo_coupon_use"

            Request = WebRequest.Create(Link)
            Request.Method = "POST"
            Request.ContentType = "application/json"
            Request.Timeout = 30000

            RawData = ("{ " &
                """ServiceVersion"":""" & ServiceVersion.ToString & """, " &
                """CouponCode"":""" & pCodigoCupon & """, " &
                """CouponNumber"":""" & pSerialCupon & """, " &
                """AccountCode"":""" & pCodigoCuenta & """, " &
                """ChannelID"":""" & pCanal & """, " &
                """LocationCodeUsed"":""" & pCodigoLocalidad & """, " &
                """POSCodeUsed"":""" & pCodigoPos & """, " &
                """TransactionTypeUsed"":""" & pTipoTransaccion & """, " &
                """DocumentNumberUsed"":""" & pNumeroDocumento & """, " &
                """CurrencyAmount"":""" & pMonto & """, " &
                """ExchangeRate"":""" & pExchangeRate & """, " &
                """Locale"":""" & Locale & """, " &
                """APIUser"":""" & ApiUser & """, " &
                """APIKey"":""" & ApiKey & """, " &
                """Hipersimulation"":""55GMFD26PF2A2B6DS"" " +
                "}")

            Data = "{" &
                """Request"":""Initialize"", " &
                """Ancestor"":""Standard"", " &
                """Payload"":""" & DataEncryption.EncryptTextADV(RawData, IIf(OverrideSrvKey.Trim.Length <= 0, EncryptionKey + SharedAppK, OverrideSrvKey)) & """ " &
                "}"

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage(Replace(RawData, ", " &
                    """APIUser"":""" & ApiUser & """, " &
                    """APIKey"":""" & ApiKey & """, " &
                    """Hipersimulation"":""55GMFD26PF2A2B6DS""",
                    String.Empty, 1, 1, CompareMethod.Text))
                'VB6Aux.ShowBigDebugMessage(RawData)
                'VB6Aux.ShowBigDebugMessage(Data)
            End If

            RawData = String.Empty

            Dim encoding As New System.Text.UTF8Encoding()
            Dim bytes As Byte() = encoding.GetBytes(Data)

            Using requestStream As Stream = Request.GetRequestStream()
                'Send the data.
                requestStream.Write(bytes, 0, bytes.Length)
            End Using

            Response = Request.GetResponse

            RespHttp = CType(Response, HttpWebResponse).StatusCode

            If DebugMode Then
                MsgBox(RespHttp.ToString(), MsgBoxStyle.Information, "DebugMode - Resp. HttpStatusCode")
            End If

            If RespHttp = HttpStatusCode.Created Then

                Dim dataStream As New StreamReader(Response.GetResponseStream)
                Dim result As String = dataStream.ReadToEnd

                Dim resp As JsonResp_ConsumirCupon
                resp = JsonConvert.DeserializeObject(Of JsonResp_ConsumirCupon)(result)

                Codigo = "00"
                Mensaje = "Anulación Exitosa"
                Referencia = resp.couponNumber

                Return True

            Else

                Dim DataStream As New StreamReader(Response.GetResponseStream)
                Dim Result As String = DataStream.ReadToEnd

                If (ServiceVersion < 2) Then

                    Dim respV1 As JsonResp_Fail
                    respV1 = JsonConvert.DeserializeObject(Of JsonResp_Fail)(Result)

                    If (respV1._error) Then
                        Codigo = "99" ' Temporal mientras se pide el codigo.
                        Mensaje = respV1.message + IIf(Not String.IsNullOrEmpty(respV1.errorMessage), " - " + respV1.errorMessage, String.Empty)
                    Else
                        Codigo = "10" ' Temporal mientras se pide el codigo.
                        Mensaje = respV1.message + IIf(Not String.IsNullOrEmpty(respV1.errorMessage), " - " + respV1.errorMessage, String.Empty)
                    End If

                ElseIf (ServiceVersion >= 2) Then

                    Dim RespV2 As JsonResp_FailV2

                    RespV2 = JsonConvert.DeserializeObject(Of JsonResp_FailV2)(Result)

                    Codigo = RespV2.RespCode
                    Mensaje = RespV2.message

                End If

                Return False

            End If

        Catch ex As Exception

            Codigo = "-1"
            Mensaje = "Falla de comunicación o timeout al intentar anular consumo de cupón. " +
                "Información adicional: " + vbNewLine + vbNewLine + Err.Description + " [" + Err.Number.ToString() + "]"
            Return False

        End Try

    End Function

End Class
