﻿Imports System
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Dynamic
Imports System.Reflection

Namespace InterfazCupones.Clases

    Class Functions

        'Public Shared EncryptionKey As String

        Public Shared gPath As String = Assembly.GetExecutingAssembly().Location.Replace(Assembly.GetExecutingAssembly().GetName().Name + ".dll", String.Empty)

        'public static FSLogger gLogger = Logger;
        'public static FSLogger Logger = New FSLogger(System.Environment.CurrentDirectory + "\\Log.txt");
        Public Shared Logger As FSLogger = New FSLogger(gPath + "Log.txt")

        Private Sub New()
            'EncryptionKey = My.Settings.EncryptionKey
        End Sub

        Public Shared Function getAlternateTrustedConnectionString(ByVal ServerInstance As String, ByVal DBName As String) As String
            Return "Server=" & ServerInstance & ";Database=" & DBName & ";Trusted_Connection=True;"
        End Function

        Public Shared Function getAlternateTrustedConnection(ByVal ServerInstance As String, ByVal DBName As String, ByVal Optional ConnectionTimeout As Integer = -1) As SqlConnection
            Return New SqlConnection(getAlternateTrustedConnectionString(ServerInstance, DBName) & (If((ConnectionTimeout <> -1), "Connection Timeout = " & ConnectionTimeout & ";", String.Empty)))
        End Function

        Public Shared Function getAlternateConnectionString(ByVal ServerInstance As String, ByVal DBName As String, ByVal UserID As String, ByVal Password As String, ByVal Optional ConnectionTimeout As Integer = -1) As String
            Return "Server=" & ServerInstance & ";Database=" & DBName & ";User Id=" & UserID & ";Password=" & Password & ";"
        End Function

        Public Shared Function getAlternateConnection(ByVal ServerInstance As String, ByVal DBName As String, ByVal UserID As String, ByVal Password As String, ByVal Optional ConnectionTimeout As Integer = -1) As SqlConnection
            Return New SqlConnection(getAlternateConnectionString(ServerInstance, DBName, UserID, Password) & (If((ConnectionTimeout <> -1), "Connection Timeout = " & ConnectionTimeout & ";", String.Empty)))
        End Function

        Public Shared Function CheckDBConnection(ByVal pDBConnection As SqlConnection) As Boolean
            Dim Returns As Boolean = False

            Try
                Dim command As SqlCommand = New SqlCommand("SELECT GETDATE() AS ServerTime", pDBConnection)
                pDBConnection.Open()
                Dim reader As SqlDataReader = command.ExecuteReader()

                If reader.HasRows Then
                    reader.Read()
                    Returns = True
                End If

                reader.Close()
                Return Returns
            Catch ex As Exception
                Console.WriteLine(ex.Message)
                Return False
            Finally
                pDBConnection.Close()
            End Try
        End Function

        Public Function getServerDatetime(ByVal DBConnection As SqlConnection) As DateTime
            Dim returns As DateTime = DateTime.Now

            Try
                Dim command As SqlCommand = New SqlCommand("SELECT GETDATE() AS ServerTime", DBConnection)
                DBConnection.Open()
                Dim reader As SqlDataReader = command.ExecuteReader()

                If reader.HasRows Then
                    reader.Read()
                    returns = Convert.ToDateTime(reader("ServerTime"))
                End If

                reader.Close()
                Return returns
            Catch ex As Exception
                Console.WriteLine(ex.Message)
                Return returns
            Finally
                DBConnection.Close()
            End Try
        End Function

        Public Shared Function getParameterizedCommand(ByVal Params As SqlParameter()) As SqlCommand
            Dim returnCommand As SqlCommand = New SqlCommand()
            If Params IsNot Nothing Then returnCommand.Parameters.AddRange(Params)
            Return returnCommand
        End Function

        Public Shared Function getParameterizedCommand(ByVal CommandText As String, ByVal Params As SqlParameter()) As SqlCommand
            Dim returnCommand As SqlCommand = New SqlCommand(CommandText)
            If Params IsNot Nothing Then returnCommand.Parameters.AddRange(Params)
            Return returnCommand
        End Function

        Public Shared Function getParameterizedCommand(ByVal CommandText As String, ByVal Connection As SqlConnection, ByVal Params As SqlParameter()) As SqlCommand
            Dim returnCommand As SqlCommand = New SqlCommand(CommandText, Connection)
            If Params IsNot Nothing Then returnCommand.Parameters.AddRange(Params)
            Return returnCommand
        End Function

        Public Shared Function getParameterizedCommand(ByVal CommandText As String, ByVal Connection As SqlConnection, ByVal Transaction As SqlTransaction, ByVal Params As SqlParameter()) As SqlCommand
            Dim returnCommand As SqlCommand = New SqlCommand(CommandText, Connection, Transaction)
            returnCommand.Transaction = Transaction
            If Params IsNot Nothing Then returnCommand.Parameters.AddRange(Params)
            Return returnCommand
        End Function

        Public Shared Function ByteToHex2(ByVal Bin As Byte()) As String

            Dim Result As String = String.Empty

            For i As Integer = 0 To Bin.Length - 1
                Result += Bin(i).ToString("X2")
            Next

            Return (Result)

        End Function

        Public Shared Function Hex2ToByte(ByVal Text As String) As Byte()

            If Text Is Nothing Then Return Nothing

            Dim Bin As Byte() = New Byte(Text.Length / 2 - 1) {}

            For i As Integer = 0 To bin.Length - 1
                Bin(i) = Convert.ToByte(Text.Substring(i * 2, 2), 16)
            Next

            Return Bin

        End Function

        Public Shared Function Tab() As String
            Return vbTab
        End Function

        Public Shared Function Tab(ByVal HowMany As Integer) As String
            Dim Tabs As String = String.Empty

            For i As Integer = 1 To HowMany
                Tabs += Tab()
            Next

            Return Tabs
        End Function

        Public Shared Function NewLine() As String
            Return System.Environment.NewLine
        End Function

        Public Shared Function NewLine(ByVal HowMany As Integer) As String
            Dim Lines As String = String.Empty

            For i As Integer = 1 To HowMany
                Lines += System.Environment.NewLine
            Next

            Return Lines
        End Function

        Public Shared Function getColorOrEmpty(ByVal ColorInput As String) As System.Drawing.Color
            Dim MyColor As System.Drawing.Color = System.Drawing.Color.Empty
            Dim ColorNumber As Integer
            Dim ColorDetails As String()

            Try
                ColorDetails = ColorInput.Split(","c)
            Catch ex As Exception
                Return MyColor
            End Try

            If ColorDetails.Length = 4 Then

                Try
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails(0)), Convert.ToInt32(ColorDetails(1)), Convert.ToInt32(ColorDetails(2)), Convert.ToInt32(ColorDetails(3)))
                Catch ex As Exception
                    MyColor = System.Drawing.Color.Empty
                    Return MyColor
                End Try

            End If

            If ColorDetails.Length = 3 Then

                Try
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails(0)), Convert.ToInt32(ColorDetails(1)), Convert.ToInt32(ColorDetails(2)))
                Catch ex As Exception
                    MyColor = System.Drawing.Color.Empty
                    Return MyColor
                End Try

            End If

            If ColorDetails.Length = 1 Then

                If Integer.TryParse(ColorDetails(0), ColorNumber) Then

                    Try
                        MyColor = System.Drawing.Color.FromArgb(ColorNumber)
                    Catch ex As Exception
                        MyColor = System.Drawing.Color.Empty
                        Return MyColor
                    End Try
                Else

                    If ColorDetails(0).Contains("#") Then

                        Try
                            MyColor = System.Drawing.ColorTranslator.FromHtml(ColorDetails(0))
                        Catch ex As Exception
                            MyColor = System.Drawing.Color.Empty
                            Return MyColor
                        End Try
                    Else

                        Try
                            MyColor = System.Drawing.Color.FromName(ColorDetails(0))
                        Catch ex As Exception
                            MyColor = System.Drawing.Color.Empty
                            Return MyColor
                        End Try
                    End If
                End If
            End If

            Return MyColor
        End Function

        Public Shared Function getColorOrDefault(ByVal ColorInput As String, ByVal FallBackColor As System.Drawing.Color) As System.Drawing.Color

            Dim MyColor As System.Drawing.Color = FallBackColor
            Dim ColorNumber As Integer
            Dim ColorDetails As String()

            Try
                ColorDetails = ColorInput.Split(","c)
            Catch ex As Exception
                Return MyColor
            End Try

            If ColorDetails.Length = 4 Then

                Try
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails(0)), Convert.ToInt32(ColorDetails(1)), Convert.ToInt32(ColorDetails(2)), Convert.ToInt32(ColorDetails(3)))
                Catch ex As Exception
                    MyColor = FallBackColor
                    Return MyColor
                End Try
            End If

            If ColorDetails.Length = 3 Then

                Try
                    MyColor = System.Drawing.Color.FromArgb(Convert.ToInt32(ColorDetails(0)), Convert.ToInt32(ColorDetails(1)), Convert.ToInt32(ColorDetails(2)))
                Catch ex As Exception
                    MyColor = FallBackColor
                    Return MyColor
                End Try
            End If

            If ColorDetails.Length = 1 Then

                If Integer.TryParse(ColorDetails(0), ColorNumber) Then

                    Try
                        MyColor = System.Drawing.Color.FromArgb(ColorNumber)
                    Catch ex As Exception
                        MyColor = FallBackColor
                        Return MyColor
                    End Try
                Else

                    If ColorDetails(0).Contains("#") Then

                        Try
                            MyColor = System.Drawing.ColorTranslator.FromHtml(ColorDetails(0))
                        Catch ex As Exception
                            MyColor = FallBackColor
                            Return MyColor
                        End Try
                    Else

                        Try
                            MyColor = System.Drawing.Color.FromName(ColorDetails(0))
                        Catch ex As Exception
                            MyColor = FallBackColor
                            Return MyColor
                        End Try
                    End If
                End If
            End If

            Return MyColor

        End Function

        Public Shared Function GetDirectorySize(ByVal FolderPath As String) As Long
            Try
                Dim AllFilesWithinFolder As String() = System.IO.Directory.GetFiles(FolderPath, "*", System.IO.SearchOption.AllDirectories)
                Dim TotalBytes As Long = 0

                For Each FilePath As String In AllFilesWithinFolder
                    Dim FileData As System.IO.FileInfo = New System.IO.FileInfo(FilePath)
                    TotalBytes += FileData.Length
                Next

                Return TotalBytes
            Catch Any As Exception
                Console.WriteLine(Any.Message)
                Return 0
            End Try
        End Function

        Public Shared Function PeopleFriendlyFormattedSize(ByVal Bytes As Long) As String

            If Bytes <= 0 Then Return "0|B"

            If Bytes > 1 AndAlso Bytes < 1000 Then
                Return Bytes.ToString() & "|" & "B"
            ElseIf Bytes >= 1000 AndAlso Bytes <= 1024 Then
                Return "1|KB"
            End If

            Dim Result As Double = (CDbl(Bytes) / 1024)

            If Result > 1 AndAlso Result < 1000 Then
                Return Math.Round(Result, 2).ToString() & "|" & "KB"
            ElseIf Result >= 1000 AndAlso Result <= 1024 Then
                Return "1|MB"
            End If

            Result = (Result / 1024)

            If Result > 1 AndAlso Result < 1000 Then
                Return Math.Round(Result, 2).ToString() & "|" & "MB"
            ElseIf Result >= 1000 AndAlso Result <= 1024 Then
                Return "1|GB"
            End If

            Result = (Result / 1024)

            If Result > 1 AndAlso Result < 1000 Then
                Return Math.Round(Result, 2).ToString() & "|" & "GB"
            ElseIf Result >= 1000 AndAlso Result <= 1024 Then
                Return "1|TB"
            End If

            Result = (Result / 1024)

            If Result > 1 AndAlso Result < 1000 Then
                Return Math.Round(Result, 2).ToString() & "|" & "MB"
            End If

            Return "0|B"

        End Function

        Public Shared Function RealFormattedSize(ByVal Bytes As Long) As String

            If Bytes <= 0 Then Return "0|B"
            Dim Result As Double = (CDbl(Bytes) / 1024)

            If Result < 1 Then
                Return Bytes.ToString() & "|" & "B"
            End If

            If Result > 1 AndAlso Result < 1024 Then
                Return Math.Round(Result, 2).ToString() & "|" & "KB"
            End If

            Result = (Result / 1024)

            If Result >= 1 AndAlso Result < 1024 Then
                Return Math.Round(Result, 2).ToString() & "|" & "MB"
            End If

            Result = (Result / 1024)

            If Result >= 1 AndAlso Result < 1024 Then
                Return Math.Round(Result, 2).ToString() & "|" & "GB"
            End If

            Result = (Result / 1024)

            If Result >= 1 AndAlso Result < 1024 Then
                Return Math.Round(Result, 2).ToString() & "|" & "TB"
            End If

            Return "0|B"

        End Function

        Public Shared Function HttpPost(ByVal URI As String, ByVal Parameters As String) As String
            Try
                Dim req As System.Net.WebRequest = System.Net.WebRequest.Create(URI)
                req.ContentType = "application/x-www-form-urlencoded"
                req.Method = "POST"
                Dim bytes As Byte() = System.Text.Encoding.ASCII.GetBytes(Parameters)
                req.ContentLength = bytes.Length
                Dim os As System.IO.Stream = req.GetRequestStream()
                os.Write(bytes, 0, bytes.Length)
                os.Close()
                Dim resp As System.Net.WebResponse = req.GetResponse()
                If resp Is Nothing Then Return Nothing
                Dim sr As System.IO.StreamReader = New System.IO.StreamReader(resp.GetResponseStream())
                Return sr.ReadToEnd().Trim()
            Catch e As System.Exception
                Console.WriteLine(e.Message)
                Return Nothing
            End Try
        End Function

        Public Shared Function SafeCreateObject(ByVal pClass As String, ByVal Optional pServerName As String = "") As Object
            Try
                Dim ObjType As Type = Nothing

                If pServerName.Trim().Length = 0 Then
                    ObjType = Type.GetTypeFromProgID(pClass, True)
                Else
                    ObjType = Type.GetTypeFromProgID(pClass, pServerName, True)
                End If

                Dim ObjInstance As ExpandoObject = Activator.CreateInstance(ObjType)
                Return ObjInstance
            Catch Any As Exception
                Logger.EscribirLog(Any, "No se pudo instanciar el objeto [" & pClass & "]")
                Return Nothing
            End Try
        End Function

        Public Shared Function ConvertirCadenadeAsociacion(ByVal pCadena As String, ByVal Optional pSeparador As Char = "|"c) As Dictionary(Of String, String)
            Try
                Dim Tmp As Dictionary(Of String, String) = New Dictionary(Of String, String)()
                Dim ParClaveValor As String() = Nothing
                Dim Split1 As String() = pCadena.Split(New Char() {pSeparador})

                For Each Item In Split1
                    ParClaveValor = Item.ToString().Split(New Char() {":"c}, 2)
                    If Not Tmp.ContainsKey(ParClaveValor(0)) Then Tmp.Add(ParClaveValor(0), ParClaveValor(1))
                Next

                If Tmp.Count <= 0 Then Throw New Exception("NoData")
                Return Tmp
            Catch Any As Exception
                Return Nothing
            End Try
        End Function

        Public Shared Function ConvertirCadenadeAsociacion(ByVal pCadena As String) As Dictionary(Of String, String)
            Return ConvertirCadenadeAsociacion(pCadena, "|"c)
        End Function

        'Public Shared Function ExisteTabla(ByVal pTabla As String, ByRef pCn As ADODB.Connection, ByVal Optional pBD As String = "") As Boolean
        '    Dim pRs As ADODB.Recordset = Nothing
        '    Dim mBD As String = (If(Not pBD.isUndefined(), pBD & ".", pBD))
        '    Dim Records As Object
        '    Dim ExisteTabla As Boolean = False

        '    Try
        '        pRs = pCn.Execute("SELECT Tabla_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'", Records)
        '        ExisteTabla = Not (pRs.EOF AndAlso pRs.BOF)
        '    Catch Any As Exception
        '    End Try

        '    Return ExisteTabla
        'End Function

        'Public Shared Function ExisteCampoTabla(ByVal pColumna As String, ByVal pTabla As String, ByRef pCn As ADODB.Connection, ByVal Optional pBD As String = "") As Boolean
        '    Dim pRs As ADODB.Recordset = Nothing
        '    Dim mBD As String = (If(Not pBD.isUndefined(), pBD & ".", pBD))
        '    Dim Records As Object
        '    Dim ExisteCampoTabla As Boolean = False

        '    Try
        '        pRs = pCn.Execute("SELECT Column_Name FROM " & mBD & "INFORMATION_SCHEMA.COLUMNS WHERE Table_Name = '" & pTabla & "' AND Column_Name = '" & pColumna & "'", Records)
        '        ExisteCampoTabla = Not (pRs.EOF AndAlso pRs.BOF)
        '    Catch Any As Exception
        '    End Try

        '    Return ExisteCampoTabla
        'End Function

        'Public Shared Function ExisteCampoRs(ByVal pRs As ADODB.Recordset, ByVal pCampo As String) As Boolean
        '    Try
        '        Dim ValorTmp As Dynamic = pRs.Fields(pCampo).Value
        '        Return True
        '    Catch Any As Exception
        '        Return False
        '    End Try
        'End Function

        'Public Shared Function Correlativo(ByVal pCn As ADODB.Connection, ByVal pCampo As String) As String
        '    Dim mRs As ADODB.Recordset = New ADODB.Recordset()
        '    Dim mSQL As String
        '    Dim mValor As String

        '    Try
        '        mSQL = "SELECT * FROM MA_CORRELATIVOS WHERE (CU_Campo = '" & pCampo & "')"
        '        mRs.CursorLocation = ADODB.CursorLocationEnum.adUseServer
        '        mRs.Open(mSQL, pCn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic)

        '        If Not mRs.EOF Then
        '            mRs.Fields("nu_Valor").Value = mRs.Fields("nu_Valor").Value + 1
        '            mValor = Microsoft.VisualBasic.Strings.Format(mRs.Fields("nu_Valor").Value, mRs.Fields("cu_Formato").Value)
        '            mRs.UpdateBatch()
        '        Else
        '            mRs.AddNew()
        '            mRs.Fields("cu_Campo").Value = pCampo
        '            mRs.Fields("cu_Formato").Value = "0000000000"
        '            mRs.Fields("nu_Valor").Value = 1
        '            mValor = Microsoft.VisualBasic.Strings.Format(mRs.Fields("nu_Valor").Value, mRs.Fields("cu_Formato").Value)
        '            mRs.UpdateBatch()
        '        End If

        '        Return mValor
        '    Catch Ex As Exception
        '        Logger.EscribirLog(Ex, "Functions.Correlativo()")
        '        'isAGENT_SxSBO.Database_Sync_Classes.DBSync.InsertarAuditoria(Program.LogIDAuditoria, Program.ErrorLog, "Error obteniendo correlativo de ejecución de Agente.", "Functions.Correlativo()", String.Empty, String.Empty, Program.mCnLocal)
        '        Return String.Empty
        '    End Try
        'End Function

        Public Shared Function isDBNull(ByVal pValue As Object) As Boolean
            Return (pValue Is Nothing)
        End Function

        Public Shared Function isDBNull(ByVal pValue As Object, ByVal pDefaultValueReturned As Object) As Object
            If isDBNull(pValue) Then
                Return pDefaultValueReturned
            Else
                Return pValue
            End If
        End Function
    End Class

End Namespace
