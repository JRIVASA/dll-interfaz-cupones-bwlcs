﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.IO

Namespace InterfazCupones.Clases

    Public Class FSLogger

        Private FileName As String

        Public Sub New(ByVal File As String)
            FileName = File
        End Sub

        Public Sub New()
            FileName = "C:/log.txt"
        End Sub

        Public Sub EscribirLog(ByVal LogText As String)
            Try

                Using w As StreamWriter = File.AppendText(FileName)
                    w.WriteLine(DateTime.Now.ToString() & " - " & LogText)
                End Using

            Catch
            End Try
        End Sub

        Public Sub EscribirLog(ByVal ex As Exception, ByVal Optional PreMsg As String = "")

            Try

                Using w As StreamWriter = File.AppendText(FileName)
                    w.WriteLine("--------------------------------------------------------------------------------")
                    If PreMsg IsNot Nothing Then w.WriteLine("Log Message: " & PreMsg)
                    w.WriteLine(DateTime.Now.ToString() & " - Exception (" & ex.[GetType]().FullName & ")")
                    w.WriteLine("Message: " & ex.Message)
                    w.WriteLine("Source: " & ex.Source)
                    w.WriteLine("StackTrace: " & ex.StackTrace)
                    w.WriteLine("HResult: " & ex.HResult.ToString())
                    If ex.InnerException IsNot Nothing Then w.WriteLine("InnerException: " + ex.InnerException.ToString)
                    If ex.HelpLink IsNot Nothing Then w.WriteLine("HelpLink: " & ex.HelpLink)

                    If ex.Data.Count > 0 Then
                        w.WriteLine("Additional Exception Data ------------------------------------------------------")
                        Dim Iterator = ex.Data.GetEnumerator()

                        While Iterator.MoveNext()
                            w.WriteLine(Iterator.Entry.Key.ToString() & " : " & Iterator.Entry.Value.ToString())
                        End While
                    End If

                    w.WriteLine("--------------------------------------------------------------------------------")
                    w.Close()
                End Using

            Catch
            End Try

        End Sub

    End Class

End Namespace
