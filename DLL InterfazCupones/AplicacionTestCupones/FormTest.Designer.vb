﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormTest
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_ConsumirCupon = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txt_Host = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txt_Saldo = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btn_AgregarCupon = New System.Windows.Forms.Button()
        Me.txt_Tipo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_Descripcion = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txt_NumeroCupon = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_Monto = New System.Windows.Forms.TextBox()
        Me.txt_NumDocumento = New System.Windows.Forms.TextBox()
        Me.txt_TipoTransaccion = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_Canal = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_CodPOS = New System.Windows.Forms.TextBox()
        Me.txt_CodLocalidad = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_CodigoCupon = New System.Windows.Forms.TextBox()
        Me.txt_serial = New System.Windows.Forms.TextBox()
        Me.txt_IDCuenta = New System.Windows.Forms.TextBox()
        Me.txt_CodSucursal = New System.Windows.Forms.TextBox()
        Me.txt_Pos = New System.Windows.Forms.TextBox()
        Me.txt_fechainicio = New System.Windows.Forms.TextBox()
        Me.txt_fechafin = New System.Windows.Forms.TextBox()
        Me.txt_moneda = New System.Windows.Forms.TextBox()
        Me.txt_monedaiso = New System.Windows.Forms.TextBox()
        Me.txt_mont = New System.Windows.Forms.TextBox()
        Me.txt_factor = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'btn_ConsumirCupon
        '
        Me.btn_ConsumirCupon.Location = New System.Drawing.Point(20, 189)
        Me.btn_ConsumirCupon.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_ConsumirCupon.Name = "btn_ConsumirCupon"
        Me.btn_ConsumirCupon.Size = New System.Drawing.Size(217, 27)
        Me.btn_ConsumirCupon.TabIndex = 1
        Me.btn_ConsumirCupon.Text = "Consumir Cupon"
        Me.btn_ConsumirCupon.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txt_Host)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 10)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(214, 184)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "VariablesSetup"
        '
        'txt_Host
        '
        Me.txt_Host.Location = New System.Drawing.Point(80, 26)
        Me.txt_Host.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Host.Name = "txt_Host"
        Me.txt_Host.Size = New System.Drawing.Size(113, 20)
        Me.txt_Host.TabIndex = 15
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(14, 28)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 13)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "Host"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txt_Saldo)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.btn_AgregarCupon)
        Me.GroupBox2.Controls.Add(Me.txt_Tipo)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txt_Descripcion)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 199)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Size = New System.Drawing.Size(214, 143)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Agregar Tipo Cupon"
        '
        'txt_Saldo
        '
        Me.txt_Saldo.Location = New System.Drawing.Point(90, 69)
        Me.txt_Saldo.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Saldo.Name = "txt_Saldo"
        Me.txt_Saldo.Size = New System.Drawing.Size(113, 20)
        Me.txt_Saldo.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 72)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Saldo"
        '
        'btn_AgregarCupon
        '
        Me.btn_AgregarCupon.Location = New System.Drawing.Point(16, 115)
        Me.btn_AgregarCupon.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_AgregarCupon.Name = "btn_AgregarCupon"
        Me.btn_AgregarCupon.Size = New System.Drawing.Size(186, 21)
        Me.btn_AgregarCupon.TabIndex = 8
        Me.btn_AgregarCupon.Text = "Agregar Cupon"
        Me.btn_AgregarCupon.UseVisualStyleBackColor = True
        '
        'txt_Tipo
        '
        Me.txt_Tipo.Location = New System.Drawing.Point(90, 46)
        Me.txt_Tipo.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Tipo.Name = "txt_Tipo"
        Me.txt_Tipo.Size = New System.Drawing.Size(113, 20)
        Me.txt_Tipo.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 49)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(28, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Tipo"
        '
        'txt_Descripcion
        '
        Me.txt_Descripcion.Location = New System.Drawing.Point(90, 24)
        Me.txt_Descripcion.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Descripcion.Name = "txt_Descripcion"
        Me.txt_Descripcion.Size = New System.Drawing.Size(113, 20)
        Me.txt_Descripcion.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 26)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Descripcion"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txt_NumeroCupon)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.txt_Monto)
        Me.GroupBox3.Controls.Add(Me.btn_ConsumirCupon)
        Me.GroupBox3.Controls.Add(Me.txt_NumDocumento)
        Me.GroupBox3.Controls.Add(Me.txt_TipoTransaccion)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.txt_Canal)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.txt_CodPOS)
        Me.GroupBox3.Controls.Add(Me.txt_CodLocalidad)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Location = New System.Drawing.Point(474, 10)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox3.Size = New System.Drawing.Size(253, 231)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Consumir Cupon"
        '
        'txt_NumeroCupon
        '
        Me.txt_NumeroCupon.Location = New System.Drawing.Point(126, 26)
        Me.txt_NumeroCupon.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_NumeroCupon.Name = "txt_NumeroCupon"
        Me.txt_NumeroCupon.Size = New System.Drawing.Size(110, 20)
        Me.txt_NumeroCupon.TabIndex = 33
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 162)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(37, 13)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Monto"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(16, 142)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(102, 13)
        Me.Label7.TabIndex = 31
        Me.Label7.Text = "Numero Documento"
        '
        'txt_Monto
        '
        Me.txt_Monto.Location = New System.Drawing.Point(128, 162)
        Me.txt_Monto.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Monto.Name = "txt_Monto"
        Me.txt_Monto.Size = New System.Drawing.Size(110, 20)
        Me.txt_Monto.TabIndex = 30
        '
        'txt_NumDocumento
        '
        Me.txt_NumDocumento.Location = New System.Drawing.Point(128, 140)
        Me.txt_NumDocumento.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_NumDocumento.Name = "txt_NumDocumento"
        Me.txt_NumDocumento.Size = New System.Drawing.Size(109, 20)
        Me.txt_NumDocumento.TabIndex = 29
        '
        'txt_TipoTransaccion
        '
        Me.txt_TipoTransaccion.Location = New System.Drawing.Point(127, 117)
        Me.txt_TipoTransaccion.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_TipoTransaccion.Name = "txt_TipoTransaccion"
        Me.txt_TipoTransaccion.Size = New System.Drawing.Size(110, 20)
        Me.txt_TipoTransaccion.TabIndex = 28
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(16, 117)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(90, 13)
        Me.Label15.TabIndex = 27
        Me.Label15.Text = "Tipo Transaccion"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(16, 51)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(34, 13)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Canal"
        '
        'txt_Canal
        '
        Me.txt_Canal.Location = New System.Drawing.Point(126, 49)
        Me.txt_Canal.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Canal.Name = "txt_Canal"
        Me.txt_Canal.Size = New System.Drawing.Size(110, 20)
        Me.txt_Canal.TabIndex = 25
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(16, 97)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(51, 13)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "Cod POS"
        '
        'txt_CodPOS
        '
        Me.txt_CodPOS.Location = New System.Drawing.Point(127, 94)
        Me.txt_CodPOS.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_CodPOS.Name = "txt_CodPOS"
        Me.txt_CodPOS.Size = New System.Drawing.Size(109, 20)
        Me.txt_CodPOS.TabIndex = 23
        '
        'txt_CodLocalidad
        '
        Me.txt_CodLocalidad.Location = New System.Drawing.Point(126, 72)
        Me.txt_CodLocalidad.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_CodLocalidad.Name = "txt_CodLocalidad"
        Me.txt_CodLocalidad.Size = New System.Drawing.Size(110, 20)
        Me.txt_CodLocalidad.TabIndex = 22
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 74)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 13)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Cod Localidad"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 28)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Numero Cupon"
        '
        'txt_CodigoCupon
        '
        Me.txt_CodigoCupon.Location = New System.Drawing.Point(298, 34)
        Me.txt_CodigoCupon.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_CodigoCupon.Name = "txt_CodigoCupon"
        Me.txt_CodigoCupon.Size = New System.Drawing.Size(76, 20)
        Me.txt_CodigoCupon.TabIndex = 6
        Me.txt_CodigoCupon.Text = "000000007"
        '
        'txt_serial
        '
        Me.txt_serial.Location = New System.Drawing.Point(298, 61)
        Me.txt_serial.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_serial.Name = "txt_serial"
        Me.txt_serial.Size = New System.Drawing.Size(76, 20)
        Me.txt_serial.TabIndex = 7
        Me.txt_serial.Text = "ABCD"
        '
        'txt_IDCuenta
        '
        Me.txt_IDCuenta.Location = New System.Drawing.Point(263, 84)
        Me.txt_IDCuenta.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_IDCuenta.Name = "txt_IDCuenta"
        Me.txt_IDCuenta.Size = New System.Drawing.Size(76, 20)
        Me.txt_IDCuenta.TabIndex = 8
        Me.txt_IDCuenta.Text = "000000001"
        '
        'txt_CodSucursal
        '
        Me.txt_CodSucursal.Location = New System.Drawing.Point(263, 106)
        Me.txt_CodSucursal.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_CodSucursal.Name = "txt_CodSucursal"
        Me.txt_CodSucursal.Size = New System.Drawing.Size(76, 20)
        Me.txt_CodSucursal.TabIndex = 9
        Me.txt_CodSucursal.Text = "01"
        '
        'txt_Pos
        '
        Me.txt_Pos.Location = New System.Drawing.Point(263, 129)
        Me.txt_Pos.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Pos.Name = "txt_Pos"
        Me.txt_Pos.Size = New System.Drawing.Size(76, 20)
        Me.txt_Pos.TabIndex = 10
        Me.txt_Pos.Text = "01"
        '
        'txt_fechainicio
        '
        Me.txt_fechainicio.Location = New System.Drawing.Point(263, 152)
        Me.txt_fechainicio.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_fechainicio.Name = "txt_fechainicio"
        Me.txt_fechainicio.Size = New System.Drawing.Size(159, 20)
        Me.txt_fechainicio.TabIndex = 11
        Me.txt_fechainicio.Text = "2020-07-14T00:00:00"
        '
        'txt_fechafin
        '
        Me.txt_fechafin.Location = New System.Drawing.Point(263, 175)
        Me.txt_fechafin.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_fechafin.Name = "txt_fechafin"
        Me.txt_fechafin.Size = New System.Drawing.Size(148, 20)
        Me.txt_fechafin.TabIndex = 12
        Me.txt_fechafin.Text = "9999-01-01T23:59:59"
        '
        'txt_moneda
        '
        Me.txt_moneda.Location = New System.Drawing.Point(263, 197)
        Me.txt_moneda.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_moneda.Name = "txt_moneda"
        Me.txt_moneda.Size = New System.Drawing.Size(76, 20)
        Me.txt_moneda.TabIndex = 13
        Me.txt_moneda.Text = "0"
        '
        'txt_monedaiso
        '
        Me.txt_monedaiso.Location = New System.Drawing.Point(263, 225)
        Me.txt_monedaiso.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_monedaiso.Name = "txt_monedaiso"
        Me.txt_monedaiso.Size = New System.Drawing.Size(76, 20)
        Me.txt_monedaiso.TabIndex = 14
        '
        'txt_mont
        '
        Me.txt_mont.Location = New System.Drawing.Point(263, 255)
        Me.txt_mont.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_mont.Name = "txt_mont"
        Me.txt_mont.Size = New System.Drawing.Size(76, 20)
        Me.txt_mont.TabIndex = 15
        Me.txt_mont.Text = "2000"
        '
        'txt_factor
        '
        Me.txt_factor.Location = New System.Drawing.Point(263, 281)
        Me.txt_factor.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_factor.Multiline = True
        Me.txt_factor.Name = "txt_factor"
        Me.txt_factor.Size = New System.Drawing.Size(79, 20)
        Me.txt_factor.TabIndex = 16
        Me.txt_factor.Text = "1"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(250, 306)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(123, 28)
        Me.Button1.TabIndex = 17
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FormTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(747, 373)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txt_factor)
        Me.Controls.Add(Me.txt_mont)
        Me.Controls.Add(Me.txt_monedaiso)
        Me.Controls.Add(Me.txt_moneda)
        Me.Controls.Add(Me.txt_fechafin)
        Me.Controls.Add(Me.txt_fechainicio)
        Me.Controls.Add(Me.txt_Pos)
        Me.Controls.Add(Me.txt_CodSucursal)
        Me.Controls.Add(Me.txt_IDCuenta)
        Me.Controls.Add(Me.txt_serial)
        Me.Controls.Add(Me.txt_CodigoCupon)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "FormTest"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_ConsumirCupon As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_Host As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_Saldo As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btn_AgregarCupon As System.Windows.Forms.Button
    Friend WithEvents txt_Tipo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_Descripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_NumeroCupon As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_Monto As System.Windows.Forms.TextBox
    Friend WithEvents txt_NumDocumento As System.Windows.Forms.TextBox
    Friend WithEvents txt_TipoTransaccion As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt_Canal As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_CodPOS As System.Windows.Forms.TextBox
    Friend WithEvents txt_CodLocalidad As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_CodigoCupon As System.Windows.Forms.TextBox
    Friend WithEvents txt_serial As System.Windows.Forms.TextBox
    Friend WithEvents txt_IDCuenta As System.Windows.Forms.TextBox
    Friend WithEvents txt_CodSucursal As System.Windows.Forms.TextBox
    Friend WithEvents txt_Pos As System.Windows.Forms.TextBox
    Friend WithEvents txt_fechainicio As System.Windows.Forms.TextBox
    Friend WithEvents txt_fechafin As System.Windows.Forms.TextBox
    Friend WithEvents txt_moneda As System.Windows.Forms.TextBox
    Friend WithEvents txt_monedaiso As System.Windows.Forms.TextBox
    Friend WithEvents txt_mont As System.Windows.Forms.TextBox
    Friend WithEvents txt_factor As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class
