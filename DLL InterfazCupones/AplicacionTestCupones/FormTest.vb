﻿Imports InterfazCupones

Public Class FormTest

    Dim ClsInt As New ClassCuponesStellar

    Private Sub DatosSetup()

        txt_Host.Text = "http://www.coupongift.xyz:5000/api"

        ClsInt.Host = txt_Host.Text

        ClsInt.Prop_Who = "513114R"
        ClsInt.Prop_ApiUser = "Tmp123456"
        ClsInt.Prop_ApiKey = "65b8cf02-8ced-4dc7-a35d-b7bbc586fab9"

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DatosSetup()
    End Sub

    Private Sub btn_AgregarCupon_Click(sender As Object, e As EventArgs) Handles btn_AgregarCupon.Click
        Dim Resp

        Resp = ClsInt.AgregarTipoCupon(txt_Descripcion.Text, txt_Tipo.Text, False)

        If Resp = True Then
            MsgBox("Exito!")
        End If
    End Sub

    Private Sub btn_ConsumirCupon_Click(sender As Object, e As EventArgs) Handles btn_ConsumirCupon.Click
        Dim Resp

        Resp = ClsInt.ConsumirCupon("000000007", txt_NumeroCupon.Text, "123456789", txt_Canal.Text, txt_CodLocalidad.Text, txt_CodPOS.Text, txt_TipoTransaccion.Text, txt_NumDocumento.Text, txt_Monto.Text)

        If Resp = True Then
            MsgBox("Exito!")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim Resp

        ClsInt.Prop_ServiceVersion = 2
        ClsInt.Prop_Locale = "es-VE"
        ClsInt.Prop_OverrideSrvKey = "Interfaz_AU_MI-Credentials-MinimumSecurity.54R48DKJJHKFFS4615F4D1C534ZD89;D47$$99167_AP5-8X2438117939201"
        'ClsInt.Prop_OverrideSrvKey = ""

        Resp = ClsInt.AgregarCupon(txt_CodigoCupon.Text, txt_serial.Text, txt_IDCuenta.Text, txt_CodSucursal.Text, txt_Pos.Text, txt_fechainicio.Text, txt_fechafin.Text, txt_moneda.Text, txt_monedaiso.Text, txt_mont.Text, txt_factor.Text)

        If Resp = True Then
            MsgBox("Exito!")
        End If
    End Sub

    Private Sub txt_Host_TextChanged(sender As Object, e As EventArgs) Handles txt_Host.TextChanged
        ClsInt.Host = txt_Host.Text
    End Sub
End Class
